// function setIframeHeight(iframe) {
//     try {
//         var frameHeight = iframe.contentWindow.document.body.scrollHeight;
//         iframe.style.height = frameHeight + "px";
//     } catch (err) {
//         console.error(err);
//     }
// }

// Boolean to know if the code has fired yet. On pre-queue transitioning to the normal queue page, queueViewModel.pageReady will fire an additional time. 
var queueViewFired = false;

queueViewModel.pageReady(function (type) {
     // queueViewModel.pageReady fires a second time on a page transition from prequeue to normal queue. This is the only instance of multiple queueViewModel.pageReady fires that we know of. The events below should only happen once. 
    if (!queueViewFired) {
        queueViewFired = true;

        if (type === "before" || type === "queue") {
            $('#content').prepend(`           
                <picture>
                    <source srcset="https://assets-us-west-2.queue-it.net/bbw/userdata/waitingroom_2x.jpg?` + new Date().getTime() + `" media="(min-width: 767px)">
                    <img class="hero-image" src="https://assets-us-west-2.queue-it.net/bbw/userdata/waitingroom.jpg?` + new Date().getTime() + `" alt="">
                </picture>
            `);
        }

        $('#aUpdateEmail').html("SUBMIT");

        $('#lbCookieInfo.cookiesAllowed').html("Yay, you’re in for our big event! Want to be notified when it’s your turn? Enter your email address at the bottom of the page.");

        $('h1.logo').detach().insertBefore('#toppanel');

        $('#MainPart_lbManualUpdateWarning').wrap('<div class="update-warning-wrapper"></div>');

        $('#first-in-line').html("You're up next.");

        $('#MainPart_lbWhichIsInText').html("You can start shopping in about:");

        $('#MainPart_lbNotyfyMeText').html("Enter your email address below, and we'll let you know when it's your turn.");

        $('#lbCookieInfo.cookiesNotAllowed').html("Hold tight! If you leave this page, you'll lose your place in line.");

        $('#MainPart_inpEmailAddress').before('<label class="email-capture-label" for="MainPart_inpEmailAddress">Email Address</label>');

        $('#MainPart_inpEmailAddress').attr('placeholder', '');

        $('#pConfirmRedirect').html("Do you want to start shopping now?");

        $('#buttonConfirmRedirect').html("YES");

        if (type === "error") {
            $('.error #lbHeaderH2').html('Sorry! Your place in line has expired.');
            $('.error .warning-box .btn').html("Get in Line");
        }

        if (type === "after") {
            $('.after #lbHeaderH2').html('That’s a wrap!');
        }

        // This was facing a cross-origin error due to the iframe being hosted on AWS and the page being hosted from Queue-it. Queue-it was unsure if this would happen in production.
        
        // Move middle panel to right below error messages
        //$('#middlepanel').detach().prependTo('#content');
        // var middlePanelFrame = document.getElementById('middlepanel_iframe');
        // middlePanelFrame.onload = function () {
        //     setIframeHeight(middlePanelFrame);
        // }

        // $(window).on('resize', function () {
        //     setIframeHeight(middlePanelFrame);
        // });
    }
});